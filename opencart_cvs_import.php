<?php

function transliterate_clean($input){
    $gost = array(
        "Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","ѓ"=>"g",
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
        "Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
        "З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
        "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
        "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
        "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
        "Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
        "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
        "е"=>"e","ё"=>"yo","ж"=>"zh",
        "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        "_"=>"-","!"=>"","@"=>"","#"=>"","$"=>"","%"=>"",
        "^"=>"","&"=>"","*"=>"","("=>"",")"=>"","+"=>"",
        "="=>"","`"=>"","~"=>"","\""=>"","'"=>"","№"=>"",
        ";"=>"",":"=>"","?"=>"","["=>"","]"=>"","{"=>"",
        "}"=>"","\/"=>"","\\"=>"","."=>"",","=>""
    );
    return strtr($input, $gost);
}

function csvImport($file_url, $db)
{

    ini_set('max_execution_time', 999999);
    file_put_contents('local.csv',file_get_contents($file_url));
    $file_reader = fopen('local.csv', 'r');
    if (!$file_reader) die('Cannot open uploaded file.');

    // словарь товаров, которые надо привязывать по названию
    $select_words = array(
        '/Тренажеры/' => 484,
    );


    $row_count = 0;

    $reseted = false;
    $db_image_path = 'data/catalog/';
    $images_path = '/var/www/m-market.ru/www/image/data/catalog/';

    // загружаем таблицу коэффициентов
    $query = $db->query("SELECT * FROM price_rates");
    $rates = $query->rows;

    foreach( $rates as $rate ) {
        $rate['rate'] = (float) $rate['rate'];
        $rate['price_from'] = (float) $rate['price_from'];
        $rate['price_to'] = (float) $rate['price_to'];
    }

   
    while ($string = fgets($file_reader)) {

        if ( !$reseted ) {
            // np@codepeckers.ru: обнуляем все товары
            $db->query("UPDATE product SET quantity_new=0, date_modified=NOW()");
            // и увеличиваем на единицу дату поступления
            $db->query("UPDATE product SET new_count=new_count+1");
            $reseted = true;
        }

        $data = explode(";", $string);

      
        $model = $db->escape(iconv('cp1251', 'utf-8', $data[0]));
        $category_name = $db->escape(iconv('cp1251', 'utf-8', $data[1]));
        $product_name = $db->escape(iconv('cp1251', 'utf-8', $data[2]));
        $product_description = $db->escape(iconv('cp1251', 'utf-8', $data[3]));
        $material = $db->escape(iconv('cp1251', 'utf-8', $data[4]));
        $count_in_msk = $data[6];
        $buying_price = (float)$data[8];
        $country = $db->escape(iconv('cp1251', 'utf-8', $data[9]));
        $manufacturer = $db->escape(iconv('cp1251', 'utf-8', $data[10]));

       
        $quantity = (int)$count_in_msk;
      
        $quantity *= 100;

        // Расчет розничной цены
        foreach ( $rates as $rate ) {
            if ( $rate['price_from'] == 0 && $buying_price <= $rate['price_to'] ) {
                $price = $buying_price * $rate['rate'];
                break;
            }
            elseif ( $rate['price_to'] == 0 && $buying_price >= $rate['price_from'] ) {
                $price = $buying_price * $rate['rate'];
                break;
            }
            elseif ( $buying_price > $rate['price_from'] && $buying_price <= $rate['price_to'] ) {
                $price = $buying_price * $rate['rate'];
                break;
            }
        }

        // теперь расчитываем новогоднюю скидку
        $ny_discount_price = 0;

        $price = (int) $price;

      
        $product_query = $db->query("SELECT product_id, price, quantity, quantity_new, image FROM product WHERE model='$model'");

        if ($product_query->row) {

          
            $old_price = (int) $product_query->row['price'];
            $is_update_product_price = true;

            $discount_query = $db->query("SELECT product_special_id, price FROM product_special WHERE product_id=" . $product_query->row['product_id']);
            if ($discount_query->row) {

               
                if ($price > $discount_query->row['price'] || $price > $old_price) {
                    $db->query("DELETE FROM product_special WHERE product_id=" . $product_query->row['product_id']);

                    // новогодняя распродажа
                    if ( $ny_discount_price > 0 ) {
                        $date_start = date("Y-m-d");
                        $data = new DateTime();
                        $date_end = $data->modify('+30 day');
                        $date_end = $date_end->format('Y-m-d');

                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,10," . $ny_discount_price . ",'$date_start','$date_end')");
                        $is_update_product_price = true;
                    }
                }

                
                if ($price < $old_price) {

                    // новогодняя распродажа
                    if ( $ny_discount_price == 0 )
                        $is_update_product_price = false;
                    else
                        $is_update_product_price = true;

                    $db->query("DELETE FROM product_special WHERE product_id=" . $product_query->row['product_id']);

                    $date_start = date("Y-m-d");
                    $data = new DateTime();
                    $date_end = $data->modify('+30 day');
                    $date_end = $date_end->format('Y-m-d');

                    // новогодняя распродажа
                    if ( $ny_discount_price == 0 )
                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,10," . $price . ",'$date_start','$date_end')");
                    else
                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,10," . $ny_discount_price . ",'$date_start','$date_end')");
                }
                else if ($price >= (int)$discount_query->row['price']) {
                    $db->query( "DELETE FROM product_special WHERE product_id=" . $product_query->row['product_id'] );

                    // новогодняя распродажа
                    if ( $ny_discount_price > 0 ) {
                        $date_start = date("Y-m-d");
                        $data = new DateTime();
                        $date_end = $data->modify('+30 day');
                        $date_end = $date_end->format('Y-m-d');

                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,10," . $ny_discount_price . ",'$date_start','$date_end')");
                        $is_update_product_price = true;
                    }
                }
            } else {
                // обрабатываем случай, когда акция еще не была создана
                if ( $price >= $old_price ) {
                    $db->query("Update product SET price='$price', buying_price='$buying_price', date_modified=NOW() WHERE model='$model' LIMIT 1");
                    // новогодняя распродажа
                    if ( $ny_discount_price > 0 ) {
                        $is_update_product_price = false;
                        $date_start = date("Y-m-d");
                        $date_end = new DateTime();
                        $date_end = $date_end->modify('+30 day');
                        $date_end = $date_end->format('Y-m-d');
                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,11," . $ny_discount_price . ",'$date_start','$date_end')");
                    }
                }
                else {
                    if ( $ny_discount_price == 0 )
                        $is_update_product_price = false;
                    else
                        $is_update_product_price = true;

                    $date_start = date("Y-m-d");
                    $date_end = new DateTime();
                    $date_end = $date_end->modify('+30 day');
                    $date_end = $date_end->format('Y-m-d');

                    if ( $ny_discount_price == 0 )
                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,11," . $price . ",'$date_start','$date_end')");
                    else
                        $db->query("INSERT INTO product_special (product_id, customer_group_id,	priority,	price,	date_start, date_end) VALUES (" . $product_query->row['product_id'] . ",1,11," . $ny_discount_price . ",'$date_start','$date_end')");
                }
            }

            // если товара не было, но он появился, также отмечаем новое поступление
            $old_quantity = intval( $product_query->row['quantity'] );
            if ( $quantity > 0 && $old_quantity == 0 ) {
                $db->query("UPDATE product SET new_count=1, date_modified=NOW() WHERE model='$model' LIMIT 1");
                //echo "Restocked model $model updated from $old_quantity to  $quantity\r\n";
            }

            // проверяем изображение: если оно не было установлено, проверяем наличие файла
            if ( $product_query->row['image'] == null || empty($product_query->row['image']) ) {
                $image_path_product = get_image_value($model, $images_path, $db_image_path);
                $db->query("Update product SET image=$image_path_product, date_modified=NOW() WHERE model='$model' LIMIT 1");
            }

            // обновляем цену, закупочную цену, статус
            if ( $is_update_product_price ) {
                $db->query("Update product SET price=$price, buying_price=$buying_price, quantity_new=$quantity, date_modified=NOW() WHERE model='$model' LIMIT 1");
            }
            else
                $db->query("Update product SET buying_price=$buying_price, quantity_new=$quantity, date_modified=NOW() WHERE model='$model' LIMIT 1");

        } else {

            //New product
            $upc = rand(1000000, 9999999);
            $query = $db->query("SELECT product_id FROM product WHERE upc=$upc");
            while ($query->num_rows != 0) {
                $upc = rand(1000000, 9999999);
                $query = $db->query("SELECT product_id FROM product WHERE upc=$upc");
            }

            //ПОИСК СТРАНЫ В СПИСКЕ СТРАН если нет занесем новую страну
            if ($country) {
                $country_querry = $db->query("SELECT productcountry_id FROM productcountry WHERE name='$country'");
                if ($country_querry->row) {
                    $country_id = $country_querry->row['productcountry_id'];
                } else {
                    $db->query("INSERT INTO productcountry (name) VALUES ('$country')");
                    $country_querry = $db->query("SELECT MAX(productcountry_id) FROM productcountry");
                    $country_id = $country_querry->row['MAX(productcountry_id)'];
                }
            } else {
                $country_id = 0;
            }
            //Конец со странами


            if ($manufacturer != "") {
                $result_brand = $db->query("SELECT manufacturer_id FROM manufacturer WHERE name='$manufacturer'");
                $result_brand = $result_brand->row;
                if (($result_brand) && ($result_brand['manufacturer_id'])) {
                    $manufacturer_id = $result_brand['manufacturer_id'];
                } else {
                    $db->query("INSERT INTO manufacturer (`name`) VALUES ('$manufacturer')");
                    $test = $db->query("SELECT manufacturer_id FROM manufacturer ORDER BY manufacturer_id DESC LIMIT 1");
                    $test = $test->row;
                    $manufacturer_id = $test['manufacturer_id'];
                    $db->query("INSERT INTO manufacturer_to_store (manufacturer_id,store_id) VALUES ($manufacturer_id,0)");
                }
            } else {
                $manufacturer_id = 0;
            }


            preg_match('/^([^-]+)-([^"]+)/', $model, $m);
      
            $category_id_mapped = false;
            foreach( $select_words as $pattern => $id_pattern ) {

                // в паттерне может содержаться только 1 паттерн, либо пара "совпадение{NOT}отрицание"
                $patterns = explode( '{NOT}', $pattern );

                if ( preg_match( $patterns[0], $product_name ) ) {
                    $not_pattern_passed = true;
                    if ( count( $patterns ) == 2 && !empty( $patterns[1] ) && preg_match( $patterns[1], $product_name ) )
                        $not_pattern_passed = false;

                    if ( $not_pattern_passed ) {
                        $category_id_mapped = $id_pattern;
                        break;
                    }
                }
            }
            if ( !$category_id_mapped )
                $result_category = $db->query("SELECT c.category_id as category_id, c.parent_id as parent_id FROM category c left join category_description cd on c.category_id=cd.category_id  WHERE cd.import_name LIKE '$category_name'");
            else
                $result_category = $db->query("SELECT c.category_id as category_id, c.parent_id as parent_id FROM category c WHERE c.category_id=$category_id_mapped");

            $result_category = $result_category->row;
            if (($result_category) && ($result_category['category_id']) && ($result_category['parent_id'])) {
                $result_category2 = $db->query("SELECT category_id, parent_id FROM category WHERE category_id=" . $result_category['parent_id']);
                $result_category2 = $result_category2->row;


                $category_id = $result_category['category_id'];
                $parent1 = $result_category2['category_id'];
                $parent2 = $result_category2['parent_id'];
            } else {
                $category_id = 0;
                $parent1 = 0;
                $parent2 = 0;
            }


            //INSERT PRODUCT

            $quantity = (int)$count_in_msk;
         

            // определяем, есть ли уже изображение
            $image_path_product = get_image_value($model, $images_path, $db_image_path);

            $db->query("INSERT INTO product (`model`, quantity, quantity_new, `image`, manufacturer_id, price, buying_price, upc , status, productcountry_id, new_count, date_added, row_rand) VALUES ('" . $model . "', " . ($quantity * 100) . ", " . ($quantity * 100) . ", " . $image_path_product . ", $manufacturer_id, $price, $buying_price, $upc ,'1',$country_id, 1, NOW(), FLOOR(1+RAND()*30) )");
            $test_p_id = $db->query("SELECT product_id FROM product ORDER BY product_id DESC LIMIT 1");
            $test_p_id = $test_p_id->row;
            $product_id = $test_p_id['product_id'];
            $db->query("INSERT INTO product_to_store (product_id,store_id) VALUES ($product_id,0)");

            $sql = "Insert INTO product_description (product_id, language_id, `name`, `description`) VALUES ($product_id,2,'" . $product_name . "','" . $product_description . "')";

            $translit_cleaned_name = transliterate_clean( $product_name );
            $db->query("INSERT INTO url_alias(`query`, keyword) VALUES ('product_id=$product_id','$translit_cleaned_name')");

            //echo $sql;
            $db->query($sql);

            if ($material) {
                $db->query("INSERT INTO product_attribute (product_id,attribute_id,language_id,`text`) VALUES ($product_id, 12, 2,'$material')");
            }

            if ($country) {
                $db->query("INSERT INTO product_attribute (product_id,attribute_id,language_id,`text`) VALUES ($product_id, 13, 2,'$country')");
            }

            //CATEGORY WITH PRODUCT
            if ($category_id != 0) {
                $db->query("INSERT INTO product_to_category (product_id,category_id,main_category) VALUES ($product_id,$category_id,1)");
            }

            if ($parent1 != 0) {
                $db->query("INSERT INTO product_to_category (product_id,category_id) VALUES ($product_id,$parent1)");
            }

            if ($parent2 != 0) {
                $db->query("INSERT INTO product_to_category (product_id,category_id) VALUES ($product_id,$parent2)");
            }
        }
        $row_count++;
    }

    $db->query("UPDATE product SET quantity=quantity_new");

    return $row_count;
}

function get_image_value($model, $images_path, $db_image_path)
{
    $image_path_product = 'NULL';
    $model_parts = explode('-', $model);
    if (count($model_parts) == 2) {
        // путь к файлу вида /путь_к_сайту/image/data/catalog/группа/модель.jpg
        $test_image_path = $images_path . $model_parts[0] . "/$model.jpg";
        if (file_exists($test_image_path))
            $image_path_product = '\'' . $db_image_path . $model_parts[0] . "/$model.jpg'";return $image_path_product;
        return $image_path_product;
    }
    return $image_path_product;
}


final class MySQL
{
    private $link;

    public function __construct($hostname, $username, $password, $database)
    {
        if (!$this->link = mysql_connect($hostname, $username, $password)) {
            trigger_error('Error: Could not make a database link using ' . $username . '@' . $hostname);
        }

        if (!mysql_select_db($database, $this->link)) {
            trigger_error('Error: Could not connect to database ' . $database);
        }

        mysql_query("SET NAMES 'utf8'", $this->link);
        mysql_query("SET CHARACTER SET utf8", $this->link);
        mysql_query("SET CHARACTER_SET_CONNECTION=utf8", $this->link);
        mysql_query("SET SQL_MODE = ''", $this->link);
    }

    public function query($sql)
    {
        $resource = mysql_query($sql, $this->link);

        if ($resource) {
            if (is_resource($resource)) {
                $i = 0;

                $data = array();

                while ($result = mysql_fetch_assoc($resource)) {
                    $data[$i] = $result;

                    $i++;
                }

                mysql_free_result($resource);

                $query = new stdClass();
                $query->row = isset($data[0]) ? $data[0] : array();
                $query->rows = $data;
                $query->num_rows = $i;

                unset($data);

                return $query;
            } else {
                //return true;
                return mysql_affected_rows();
            }
        } else {
            trigger_error('Error: ' . mysql_error($this->link) . '<br />Error No: ' . mysql_errno($this->link) . '<br />' . $sql);
            exit();
        }
    }

    public function escape($value)
    {
        return mysql_real_escape_string($value, $this->link);
    }

    public function countAffected()
    {
        return mysql_affected_rows($this->link);
    }

    public function getLastId()
    {
        return mysql_insert_id($this->link);
    }

    public function __destruct()
    {
        mysql_close($this->link);
    }
}

function updateCategories( $db ) {
    // сперва отключаем все разделы, у которых нет товаров
    $sql = "UPDATE category c SET c.status=0
            WHERE (	SELECT COUNT(DISTINCT p.product_id) AS total FROM product p LEFT JOIN product_to_category p2c ON (p.product_id = p2c.product_id)
	                WHERE p2c.category_id = c.category_id ) = 0";
    $db->query($sql);

    // затем снова включаем разделы, у которых нет товаров, но есть подразделы с товарами
    $sql = "UPDATE category c SET c.status=1
            WHERE (	SELECT COUNT(DISTINCT p.product_id) AS total FROM product p LEFT JOIN product_to_category p2c ON (p.product_id = p2c.product_id)
                WHERE p2c.category_id = c.category_id ) > 0";
    $db->query($sql);

    // на случай, если какие-то разделы без привязанных товаров, повторяем столько раз, сколько у нас уровней разделов,
    // запрос, определяющий включить ли раздел по его подразделам.
    $sql = "UPDATE category SET status=1
            WHERE category_id IN ( SELECT parent_id FROM category_copy WHERE status = 1 )";
    $db->query($sql);
    $db->query($sql);
    $db->query($sql);

    // отключаем старые разделы
    $sql = "UPDATE category SET status=0 WHERE category_id < 435";
    $db->query($sql);
}

function updateOrders( $db ) {
    $sql = "UPDATE `order` SET highlight=0";
    $db->query($sql);
    $sql = "UPDATE `order` SET highlight=1 WHERE order_status_id=17 AND order_id IN (SELECT order_id FROM order_product WHERE product_id IN (SELECT product_id FROM product WHERE quantity>0) )";
    $db->query($sql);
}

function microtime_float(){
    if ( version_compare(phpversion(), '5.0.0', '>=') ) {
        return microtime(true);
    } else {
        list($usec, $sec) = explode(' ', microtime());
        return ((float) $usec + (float) $sec);
    }
}

// Засекаем время
$time_start = microtime_float(true);

$user_name = "";
$pass = "";
$host = "localhost";
$database = "";

$file_url = "";

$db = new MySQL($host, $user_name, $pass, $database);
$rows_count = csvImport($file_url, $db);
updateCategories($db);
updateOrders($db);
unset($db);

$time_end = microtime_float(true);
$time = $time_end - $time_start;
$cur_date = date("Y-m-d H:i:s");

echo "done $cur_date, $time seconds, rows processed: $rows_count";
?>	